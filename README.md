## wot-is-dis

This is an R Shiny application to detect objects in images using the YOLO object
detection library (https://pjreddie.com/darknet/yolo/).

This is currently hosted via shinyapps.io here: https://jeremyyeo.shinyapps.io/wot-is-dis
